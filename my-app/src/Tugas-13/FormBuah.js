import React from 'react';

class FormBuah extends React.Component{
	constructor(props){
	    super(props)
	    this.state = {
	      inputName: "",
	      inputWeight: "",
	      inputPrice: "",
	      dataHargaBuah: [
	        {nama: "Semangka", harga: 10000, berat: 1000},
	        {nama: "Anggur", harga: 40000, berat: 500},
	        {nama: "Strawberry", harga: 30000, berat: 400},
	        {nama: "Jeruk", harga: 30000, berat: 1000},
	        {nama: "Mangga", harga: 30000, berat: 500}
	      ]
	    }
	    this.inputNameChange = this.inputNameChange.bind(this);
	    this.inputWeightChange = this.inputWeightChange.bind(this);
	    this.inputPriceChange = this.inputPriceChange.bind(this);
  		this.handleSubmit = this.handleSubmit.bind(this);
  		this.handleDelete = this.handleDelete.bind(this);
  		this.handleEdit = this.handleEdit.bind(this);
	}

	inputNameChange(event){
	  this.setState({
	  	inputName: event.target.value
	  });
	}

	inputWeightChange(event){
	  this.setState({
	  	inputWeight: event.target.value
	  });
	}

	inputPriceChange(event){
	  this.setState({
	  	inputPrice: event.target.value
	  });
	}

	handleSubmit(event){
	  event.preventDefault()
	  this.setState({
	    dataHargaBuah: [...this.state.dataHargaBuah, {nama: this.state.inputName, harga: this.state.inputPrice, berat: this.state.inputWeight}],
	    inputName: "",
	    inputWeight: "",
	    inputPrice: "",
	  })
	}

	handleDelete(event){
		let array = [...this.state.dataHargaBuah];
		let index = event.target.id;
		if (index !== -1) {
		    array.splice(index, 1);
		    this.setState({dataHargaBuah: array});
		  }
		console.log(event.target.id);
	}

	handleEdit(event){
		let array = [...this.state.dataHargaBuah];
		let selected = array[event.target.id];
		this.setState({
			inputName: selected.nama,
			inputWeight: selected.berat,
			inputPrice: selected.harga,
		});
		console.log(array);
		console.log(selected);
	}

	render() {
		return (
			<div className="container">
			    <form onSubmit={this.handleSubmit}>
				    <h1>Form Buah</h1>
				    <table>
				      <tr>
				        <td><b>Nama Buah</b></td>
				        <td><input value={this.state.inputName} type="text" name="nama" onChange={this.inputNameChange} /></td>
				      </tr>
				      <tr>
				        <td><b>Berat Buah</b></td>
				        <td><input value={this.state.inputWeight} type="text" name="berat" onChange={this.inputWeightChange} /></td>
				      </tr>
				      <tr>
				        <td><b>Harga Buah</b></td>
				        <td><input value={this.state.inputPrice} type="text" name="hara" onChange={this.inputPriceChange} /></td>
				      </tr>
				      <tr>
				        <td><input type="submit" value="submit" /></td>
				      </tr>
				    </table>
			    </form>
			    <table className="myTable">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Harga</th>
						<th>Berat</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					{
						this.state.dataHargaBuah.map((item,index)=>{
							return(
								<tr>
									<td> {item.nama} </td>
									<td> {item.harga} </td>
									<td> {item.berat} </td>
									<td> <button id={index} onClick={this.handleDelete}>Delete</button> <button id={index} onClick={this.handleEdit}>Edit</button> </td>
								</tr>
							)
						})
					}
				</tbody>
			</table>
			</div>
		)
	}
	
}

export default FormBuah