import React, {useState, useEffect} from 'react';
import axios from 'axios';

let FormBuah = () => {
	const [inputName, setInputName] = useState("");
	const [inputWeight, setInputWeight] = useState("");
	const [inputPrice, setInputPrice] = useState("");
	const [dataHargaBuah, setDataHargaBuah] = useState([{}]);

	const inputNameChange = (event) => {
	  setInputName(event.target.value);
	}

	const inputWeightChange = (event) => {
	  setInputWeight(event.target.value);
	}

	const inputPriceChange = (event) => {
	  setInputPrice(event.target.value);
	}

	const handleSubmit = (event) => {
	  event.preventDefault();
	  axios.post("http://backendexample.sanbercloud.com/api/fruits",{inputName,inputPrice,inputWeight})
	  	.then(()=>{
	  		setInputName("");
			setInputWeight("");
			setInputPrice("");
	  	})
	}

	const handleDelete = (event) => {
		let array = [...dataHargaBuah];
		let index = event.target.id;
		if (index !== -1) {
		    array.splice(index, 1);
		    setDataHargaBuah({dataHargaBuah: array});
		    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${index}`);
		  }
		console.log(event.target.id);
	}

	const handleEdit = (event) => {
		let array = [...this.state.dataHargaBuah];
		let selected = array[event.target.id];
		this.setState({
			inputName: selected.nama,
			inputWeight: selected.berat,
			inputPrice: selected.harga,
		});
		console.log(array);
		console.log(selected);
	}

	useEffect( () => {
		axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
			.then( res=> {
				// setDataHargaBuah({res});
				console.log(res.data);
				setDataHargaBuah([...res.data]);
				// res.data.map((item)=>{
				// 	setDataHargaBuah();
				// })
			})
		console.log(dataHargaBuah);
	},[])

	return (
			<div className="container">
			    <form onSubmit={handleSubmit}>
				    <h1>Form Buah</h1>
				    <table>
				      <tr>
				        <td><b>Nama Buah</b></td>
				        <td><input value={inputName} type="text" name="nama" onChange={inputNameChange} /></td>
				      </tr>
				      <tr>
				        <td><b>Berat Buah</b></td>
				        <td><input value={inputWeight} type="text" name="berat" onChange={inputWeightChange} /></td>
				      </tr>
				      <tr>
				        <td><b>Harga Buah</b></td>
				        <td><input value={inputPrice} type="text" name="hara" onChange={inputPriceChange} /></td>
				      </tr>
				      <tr>
				        <td><input type="submit" value="submit" /></td>
				      </tr>
				    </table>
			    </form>
			    <table className="myTable">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Harga</th>
						<th>Berat</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					{
						dataHargaBuah.map((item,index)=>{
							return(
								<tr>
									<td> {item.name} </td>
									<td> {item.price} </td>
									<td> {item.weight} </td>
									<td> <button id={item.id} onClick={handleDelete}>Delete</button> <button id={item.id} onClick={handleEdit}>Edit</button> </td>
								</tr>
							)
						})
					}
				</tbody>
			</table>
			</div>
		)
}

export default FormBuah