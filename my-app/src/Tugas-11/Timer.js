import React from 'react';

class Timer extends React.Component{

	state = {
		time: this.AMPM(new Date),
        counter: this.props.counter,
        show: true,
	}

	AMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'pm' : 'am';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? '0'+minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}

	componentDidMount() {
	  this.myInterval = setInterval(() => {
	    const { time,counter } = this.state
	    if (counter > 0) {
	      this.tick()
	    }
	    if (counter === 0) {
	    	this.setState({show:false});
			clearInterval(this.myInterval);
	    }
	  }, 1000)
	}

	componentWillUnmount() {
        clearInterval(this.myInterval);
    }

	tick(){
		this.setState({
	      counter: this.state.counter - 1 
	    });
	}

	render(){
		const { time,counter } = this.state
		return this.state.show? (
			<div>
                Sekarang jam : { time } <br />
                hitung mundur: { counter }
            </div>
		) : null;

	}
}

export default Timer