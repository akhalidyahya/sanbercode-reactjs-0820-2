import React from 'react';

class MyBox extends React.Component{
	render(){

		return(
			<div className="polaroid">
		  <img src={this.props.photo} alt="5 Terre" />
		  <div className="container">
		  <p><b>{this.props.gender == 'Female' ? 'Mrs': 'Mr'} {this.props.name}</b></p>
		  <p>{this.props.profession}</p>
		  <p>{this.props.age}</p>
		  </div>
		</div>
		)
	}
}

export default MyBox