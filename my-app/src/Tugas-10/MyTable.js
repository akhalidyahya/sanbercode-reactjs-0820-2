import React from 'react';
import Column from './Column';


class MyTable extends React.Component{

	render(){
		return (
			<table className="myTable">
				<thead>
					<tr>
						<th>Nama</th>
						<th>Harga</th>
						<th>Berat</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					{
						this.props.objectList.map((item,index)=>{
							return(
								<tr>
									<td> {item.nama} </td>
									<td> {item.harga} </td>
									<td> {item.berat} </td>
									<td> <button onClick={this.props.delete}>Delete</button> <button onClick={this.props.delete}>Edit</button> </td>
								</tr>
							)
						})
					}
				</tbody>
			</table>
		)

	}
}

export default MyTable